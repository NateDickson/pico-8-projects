![lexaloffle-pico8.png](https://bitbucket.org/repo/Ej7yzp/images/1883801183-lexaloffle-pico8.png)

# Pico-8 Projects

These are projects I'm working on for use in the [Pico-8](http://www.lexaloffle.com/pico-8.php) "fantasy console". Pico-8 is an entire 8-bit video game console that runs entirely inside another computer. And given how powerful modern computers are and how lightweight 8-bit consoles were, it shouldn't be surprising that a Pico-8 program can run entirely in browser. 

This repo is a lot of little things I've been working on, mostly demos to learn how to use the console. If you want to pop open a `.p8` file know that they're just [Lua](http://www.lua.org/) files with a lot of extra stuff thrown in at the end. You could probably even edit the graphical and musical stuff in a text editor, if you were brave and clever. 

If you want to work on one of these go for it! Pull requests will be run and reviewed before being accepted.